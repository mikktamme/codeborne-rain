package codeborne.week3;

public class Intersections {
    private int xIntersect;
    private int yIntersect;
    private int steps;

    public Intersections(int xIntersect, int yIntersect, int steps) {
        this.xIntersect = xIntersect;
        this.yIntersect = yIntersect;
        this.steps = steps;
    }

    public int getxIntersect() {
        return xIntersect;
    }

    public int getyIntersect() {
        return yIntersect;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "Intersections{" +
                "xIntersect=" + xIntersect +
                ", yIntersect=" + yIntersect +
                ", steps=" + steps +
                '}';
    }
}
