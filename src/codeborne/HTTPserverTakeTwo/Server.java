package codeborne.HTTPserverTakeTwo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public void webServer() throws IOException {
        ServerSocket server = new ServerSocket(8888);
        System.out.println("Server started, listening on port 8888");

        while (true) {
            ResHandler response = new ResHandler();
            try (Socket session = server.accept()) {
                response.responseHandler(session);
                session.shutdownInput();
                session.shutdownOutput();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
