package codeborne.HTTPserverTakeTwo;

public class Request {
    private String methodType;
    private String filePath;
    private String body;

    public Request(String methodType, String filePath, String body) {
        this.methodType = methodType;
        this.filePath = filePath;
        this.body = body;
    }

    public String getFilePath() {
        return filePath;
    }

    @Override
    public String toString() {
        return "Request{" +
                "type='" + methodType + '\'' +
                ", filePath='" + filePath + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
