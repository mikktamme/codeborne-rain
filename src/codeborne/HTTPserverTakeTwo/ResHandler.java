package codeborne.HTTPserverTakeTwo;

import java.io.*;
import java.net.Socket;

public class ResHandler {

    void responseHandler(Socket session) throws IOException {
        InputStream input = session.getInputStream();
        BufferedOutputStream dataOut = new BufferedOutputStream(session.getOutputStream());
        responseBuilder builder = new responseBuilder();
        try {
            ReqHandler request = new ReqHandler();
            dataOut.write(builder.builder(request.requestAsObject(input).getFilePath()));
            dataOut.flush();
        } catch (FileNotFoundException fnf) {
            dataOut.write(builder.builder("404.html"));
            dataOut.flush();
        }
    }
}
