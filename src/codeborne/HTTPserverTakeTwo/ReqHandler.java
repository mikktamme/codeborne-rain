package codeborne.HTTPserverTakeTwo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class ReqHandler {

    Request requestAsObject(InputStream inputStream) throws IOException {
        byte[] originalRequest = inputStreamToBytes(inputStream);
        return new Request
                (
                        requestMethod(splitStatusLine(headerStatusLine(splitHeaderLines(
                                getHeader(splitStringToHeadAndBody(stringToList(byteArrayToString(originalRequest)))))))),
                        requestedFile(splitStatusLine(headerStatusLine(splitHeaderLines(
                                getHeader(splitStringToHeadAndBody(stringToList(byteArrayToString(originalRequest)))))))),
                        getBody(splitStringToHeadAndBody(stringToList(byteArrayToString(originalRequest))))
                );
    }

    byte[] inputStreamToBytes(InputStream inputStream) throws IOException {
        return inputStream.readNBytes(inputStream.available());
    }

    String byteArrayToString(byte[] input) {
        return new String(input);
    }

    List<String> stringToList(String byteArrayToString) {
        return Collections.singletonList(byteArrayToString);
    }

    String[] splitStringToHeadAndBody(List<String> stringToList) {
        return stringToList.get(0).split("\r\n\r\n");
    }

    String getHeader(String[] input) {
        return input[0];
    }

    String getBody(String[] input) {
        return input.length > 1 ? input[1] : "";
    }

    String[] splitHeaderLines(String input) {
        return input.split("\r\n");
    }

    String headerStatusLine(String[] input) {
        return input[0];
    }

    String[] splitStatusLine(String input) {
        return input.split(" ");
    }

    String requestMethod(String[] input) {
        return input[0];
    }

    String requestedFile(String[] input) {
        return input.length > 1 ? input[1] : "";
    }
}

