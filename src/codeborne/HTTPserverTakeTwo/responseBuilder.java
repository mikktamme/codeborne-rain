package codeborne.HTTPserverTakeTwo;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class responseBuilder {

    byte[] builder(String filePath) throws IOException {
        int outBodyLength = readFile(requestedFile(filePath)).length;
        String headerOutString = new Response
                (
                        statusText(requestedFile(filePath)),
                        contentType(filePath),
                        outBodyLength,
                        new String(readFile(requestedFile(filePath)))
                ).resHeaderAndBodyToString();
        return headerOutString.getBytes(StandardCharsets.UTF_8);
    }

    File requestedFile(String filePath) {
        return new File("Resources", filePath.replace("/", ""));
    }

    String statusText(File requestedFile) {
        if (requestedFile.exists() && !requestedFile.getName().equals("404.html")) {
            return "200 OK";
        } else {
            return "404 File Not Found";
        }
    }

    String contentType(String filePath) {
        if (filePath.endsWith(".html")) return "text/html";
        else if (filePath.endsWith(".ico")) return "image/icon";
        else if (filePath.endsWith(".png")) return "image/png";
        else return "text/plain";
    }

    byte[] readFile(File requestedFile) throws IOException {
        FileInputStream fileIn = new FileInputStream(requestedFile);
        return fileIn.readAllBytes();
    }
}
