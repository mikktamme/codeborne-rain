package codeborne.HTTPserverTakeTwo;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Server run = new Server();
        run.webServer();
    }
}
