package codeborne.week2;

import java.util.Scanner;

public class Day07NumbersGame {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Guess a random number: 1-100");
        System.out.println("To exit the game type - Exit.");
        int randomNumberForPlayer = (int) (Math.random() * 100 + 1);
        int guessCount = 1;
        while (true) {
            try {
                String input = scanner.nextLine();
                if (input.trim().equalsIgnoreCase("exit")) {
                    System.out.println("Thank you for playing, Good bye!");
                    break;
                }
                if (Integer.parseInt(input) == randomNumberForPlayer) {
                    System.out.println(gameLogic(input, randomNumberForPlayer));
                    System.out.printf("By guessing: %d\nWhile guessing %d times", randomNumberForPlayer, guessCount);
                    break;
                }
                guessCount++;
                System.out.println(gameLogic(input, randomNumberForPlayer));
            } catch (NumberFormatException e) {
                System.out.println("Wrong input try again!");
                guessCount++;
            }
        }
    }

    public static String gameLogic(String input, int randomNumberForPlayer) {
        if (Integer.parseInt(input) < 1 || Integer.parseInt(input) > 100) {
            return "Guess must be between 1 -100- try again.";
        } else if (Integer.parseInt(input) > randomNumberForPlayer) {
            return "The number is smaller.";
        } else if (Integer.parseInt(input) < randomNumberForPlayer) {
            return "The number is bigger.";
        } else {
            return "Congrats you won!";
        }
    }
}
