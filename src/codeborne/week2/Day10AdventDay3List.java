package codeborne.week2;

import codeborne.week1.Day03NumbersReader;
import codeborne.week1.Day03QuickSort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class Day10AdventDay3List {
    public static void main(String[] args) throws IOException {
        Day10AdventDay3List advent = new Day10AdventDay3List();

//        System.out.println(advent.distanceList(
//                advent.wireToList(Day03NumbersReader.fileReaderListStr("advent03first.txt")),
//                advent.wireToList(Day03NumbersReader.fileReaderListStr("advent03Second.txt")))
//        );
        System.out.println(advent.lengthList(
                advent.wireToList(Day03NumbersReader.fileReaderListStr("advent03first.txt")),
                advent.wireToList(Day03NumbersReader.fileReaderListStr("advent03Second.txt")))
        );
    }

    private List<String> wireToList(List<String> firstList) {
        List<String> wireList = new ArrayList<>();
        int x = 0, y = 0;

        for (String s : firstList) {
            int[] pointer = rightDownLeftUpPointer(String.valueOf(s.charAt(0)));
            int numberForLength = Integer.parseInt(s.substring(1));
            for (int i = 0; i < numberForLength; i++) {
                int xPointer = x + pointer[0];
                int yPointer = y + pointer[1];
                wireList.add(xPointer + ", " + yPointer);
                x = xPointer;
                y = yPointer;
            }
        }
        return wireList;
    }

    public int distanceList(List<String> firstWire, List<String> secondWire) {
        int distance = 0;
        try {
            for (String pointer : firstWire) {
                if (secondWire.contains(pointer)) {
                    String[] splitPointer = pointer.split(", ");
                    int tempValue = abs(Integer.parseInt(splitPointer[0])) + abs(Integer.parseInt(splitPointer[1]));
                    if (tempValue < distance || distance == 0) {
                        distance = tempValue;
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 1;
        } catch (NumberFormatException e) {
            return 2;
        }
        return distance;
    }

    public int lengthList(List<String> firstWire, List<String> secondWire) {
        int sumOfSteps = 0;
        List<String> copy1 = new ArrayList<>(List.copyOf(firstWire));
        copy1.retainAll(secondWire);
        for (String s : copy1) {
            int tempValue = firstWire.indexOf(s)+secondWire.indexOf(s) + 2;
            if (tempValue < sumOfSteps || sumOfSteps == 0) {
                    sumOfSteps = tempValue;
                }
        }

//        for (int i = 0; i < firstWire.size(); i++) {
//            if (secondWire.contains(firstWire.get(i))) {
//                System.out.println(firstWire.get(i));
//                int index = secondWire.indexOf(firstWire.get(i));
//                int tempValue = i + index + 2;
//                if (tempValue < sumOfSteps || sumOfSteps == 0) {
//                    sumOfSteps = tempValue;
//                }
//            }
//        }
        return sumOfSteps;
    }

    private int[] rightDownLeftUpPointer(String inputLetter) {
        int[] pointer;
        switch (inputLetter) {
            case "R":
                pointer = new int[]{1, 0};
                break;
            case "D":
                pointer = new int[]{0, -1};
                break;
            case "L":
                pointer = new int[]{-1, 0};
                break;
            case "U":
                pointer = new int[]{0, 1};
                break;
            default:
                return new int[]{0, 0, 7};
        }
        return pointer;
    }
}

