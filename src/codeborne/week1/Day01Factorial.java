package codeborne.week1;

import java.math.BigInteger;

public class Day01Factorial {

    public static BigInteger calcFactorial(int n) {
        if (n < 0) {
            System.out.println("Negative factorial, is not covered in the scope of this exercise");
            return null;
        }
        n++;
        BigInteger var = BigInteger.valueOf(n);
        BigInteger factorial = BigInteger.valueOf(n);
        for(int i = n - 1; i >= 1; i--) {
            BigInteger variable = factorial.multiply(BigInteger.valueOf(n - i));
            factorial = (i == 1) ? variable.divide(var) : variable;
        }
        return factorial;
    }
}