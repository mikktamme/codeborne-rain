package codeborne.week1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day02Palindrome {

    public static boolean checkIfStringIsPalindrome(String input) {

        int count = 0;
        List<Character> reverseString = new ArrayList<>();
        for (int i = input.length() - 1; i >= 0; i--) {
            reverseString.add(input.charAt(i));
            if (Character.toLowerCase(input.charAt(reverseString.size() - 1)) ==
                    Character.toLowerCase(reverseString.get(reverseString.size() - 1))) {
                count++;
            }
            if (count == input.length() / 2) return true;
        }
        return false;
    }
}
