package codeborne.javaWebServer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class JavaWebServerTest {

    public static void main(String[] args) throws IOException {
        startWebServer();
    }

    private static void startWebServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8001);
        System.out.println("Server running...");

        while (true) {
            /*Socket clientSocket = serverSocket.accept();*/
            JavaWebServerTest.SocketThread socketThread = new JavaWebServerTest().new SocketThread();
            socketThread.setClientSocket(serverSocket.accept());
            Thread thread = new Thread(socketThread);
            thread.start();
        }

    }

    public class SocketThread implements Runnable {

        Socket clientSocket;

        public void setClientSocket(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        public void run() {
            System.out.println("####### New client session started.");
            try {
                listenToSocket();
            } catch (IOException e) {
                System.err.println("#### EXCEPTION.");
                e.printStackTrace();
            }
        }

        private void listenToSocket() throws IOException {
            /*BufferedReader clientSocketReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter clientSocketWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));*/

            InputStreamReader clientSocketReader = (new InputStreamReader(clientSocket.getInputStream()));
            OutputStreamWriter clientSocketWriter = (new OutputStreamWriter(clientSocket.getOutputStream()));

            int clientSocketInput = 0;
            boolean isFirstLineFeedEncountered = false;
            boolean isFirstCarriageReturnEncountered = false;
            boolean isSecondCarriageReturnEncountered = false;
            while (clientSocket.isConnected() && (clientSocketInput = clientSocketReader.read()) != 0) {
                if (isFirstCarriageReturnEncountered) {
                    System.out.println("### After first CR");
                    if (isFirstLineFeedEncountered) {
                        if (isSecondCarriageReturnEncountered) {
                            System.out.println("### 2 sets of CRLF encountered.");
                            String responseHeaders = construct_http_header(200);
                            clientSocketWriter.append(responseHeaders);
                            clientSocketWriter.append("Bye from server, thank you for contacting me!!!");
                            clientSocketWriter.append("\r\n\r\n");
                            clientSocketWriter.flush();
                            clientSocketWriter.close();
                            clientSocketReader.close();
                            clientSocket.close();
                            break;
                        } else if (clientSocketInput == 13) {
                            System.out.println("### After first set of CRLF, found another CR");
                            isSecondCarriageReturnEncountered = true;
                        } else {
                            System.out.println("### Oops, after first set of CRLF");
                            isFirstLineFeedEncountered = false;
                            isFirstCarriageReturnEncountered = false;
                            isSecondCarriageReturnEncountered = false;
                        }
                    } else if (clientSocketInput == 10) {
                        System.out.println("### First LF");
                        isFirstLineFeedEncountered = true;
                    } else {
                        isFirstCarriageReturnEncountered = false;
                    }
                }

                // identify "CR" or ASCII 13
                if (clientSocketInput == 13) {
                    System.out.println("### First CR");
                    isFirstCarriageReturnEncountered = true;
                }

                // identify "CTRL+C" or ASCII 3

                if(clientSocketInput == 3){
                    System.out.println("### Encountered CTRL+C");
                    clientSocketWriter.append("Bye from server, thank you for contacting me!!!");
                    clientSocketWriter.flush();
                    clientSocketWriter.close();
                    clientSocketReader.close();
                    clientSocket.close();
                    break;
                }
                if(clientSocketInput == 4){
                    System.out.println("### Encountered CTRL+D");
                    clientSocketWriter.append("Bye from server, thank you for contacting me!!!");
                    clientSocketWriter.flush();
                    clientSocketWriter.close();
                    clientSocketReader.close();
                    clientSocket.close();
                    break;
                }
            }

            // identify "CTRL+D" or ASCII 4
            System.out.print((char) clientSocketInput);
        }
    }

    private String construct_http_header(int return_code) {
        String s = "HTTP/1.1 ";
        // you probably have seen these if you have been surfing the web a
        // while
        switch (return_code) {
            case 200:
                s = s + "200 OK";
                break;
            case 400:
                s = s + "400 Bad Request";
                break;
            case 403:
                s = s + "403 Forbidden";
                break;
            case 404:
                s = s + "404 Not Found";
                break;
            case 500:
                s = s + "500 Internal Server Error";
                break;
            case 501:
                s = s + "501 Not Implemented";
                break;
        }

        s = s + "\r\n"; // other header fields,
        s = s + "Connection: close\r\n"; // we can't handle persistent connections
        /*s = s + "Server: SimpleHTTPtutorial v0\r\n"; // server name*/
        s = s + "Content-Type: text/plain\r\n";


        // //so on and so on......
        s = s + "\r\n"; // this marks the end of the httpheader
        // and the start of the body
        // ok return our newly created header!
        return s;
    }

}