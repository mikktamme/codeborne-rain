package codeborne.javaWebServer;

import java.util.Date;

public class Response1 {
    private String status;
    private String type;
    private int length;
    private String body;

    public Response1(String status, String type, int length) {
        this.status = status;
        this.type = type;
        this.length = length;
    }

    public Response1(String status, String type, int length, String body) {
        this.status = status;
        this.type = type;
        this.length = length;
        this.body = body;
    }

    public String responseHeader() {
        return  "HTTP/1.1 " + status + "\r\n" +
                "Server: Java HTTP Server from Rain v: 1.0" + "\r\n" +
                "Date: " + new Date() + "\r\n" +
                "Content-Type: " + type + "\r\n" +
                "Content-length: " + length + "\r\n\r\n";
    }

    public String responseHeaderAndBody() {
        return  "HTTP/1.1 " + status + "\r\n" +
                "Server: Java HTTP Server from Rain v: 1.0" + "\r\n" +
                "Date: " + new Date() + "\r\n" +
                "Content-Type: " + type + "\r\n" +
                "Content-length: " + length + "\r\n\r\n" +
                body;
    }
}
