package codeborne.javaWebServer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class JavaWebServer {

    static final File WEB_ROOT = new File("Resources");
    static final String DEFAULT_FILE = "index.html";
    static final String FILE_NOT_FOUND = "404.html";
    static final String METHOD_NOT_SUPPORTED = "501.html";

    public void webServer() throws IOException {
        final ServerSocket server = new ServerSocket(8080);
        System.out.println("Listening for connection on port 8080...");
        while (true) {
            try (Socket socket = server.accept()) {
                RequestHandler request = new RequestHandler();
                System.out.println(request.requestHandler(socket).equals("")? "tühi rida": request.requestHandler(socket));
                if (request.requestHandler(socket).equals("")){
                    socket.close();
                } else {
                    ResponseHandler response = new ResponseHandler();
                    response.responseHandler(socket);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
