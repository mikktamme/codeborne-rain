package codeborne.javaWebServer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class NotImplemented {

    void notImplemented(PrintWriter out) throws IOException {
        ResponseHandler response = new ResponseHandler();
        File file = new File(JavaWebServer.WEB_ROOT, JavaWebServer.METHOD_NOT_SUPPORTED);
        int fileLength = (int) file.length();
        String type = "text/html";
        String bodyData = String.valueOf(response.fileReader("501.html"));
        String outHeadAndBody = new Response1
                (
                        "501 Not Implemented",
                        type,
                        fileLength,
                        bodyData
                ).responseHeaderAndBody();
        out.write(outHeadAndBody);
    }
}

