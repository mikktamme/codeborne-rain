package codeborne.javaWebServer;

import java.io.IOException;

public class Launcher {
    public static void main(String[] args) throws IOException {
        JavaWebServer run = new JavaWebServer();
        run.webServer();
    }
}
