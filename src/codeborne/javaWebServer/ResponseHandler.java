package codeborne.javaWebServer;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.StringTokenizer;

import static codeborne.javaWebServer.JavaBasicHttpServer.DEFAULT_FILE;

public class ResponseHandler {

    void responseHandler(Socket socket) throws IOException {
        NotImplemented notImp = new NotImplemented();
        RequestHandler request = new RequestHandler();
        GET get = new GET();
        StringTokenizer temp = new StringTokenizer(request.requestHandler(socket));
        String requestMethod = temp.nextToken().toUpperCase();
        String requestedFile = temp.nextToken().toLowerCase();
        PrintWriter out = new PrintWriter(socket.getOutputStream());
        if (!"GET".equals(requestMethod) && !"HEAD".equals(requestMethod) && !"POST".equals(requestMethod)) {
            notImp.notImplemented(out);
        } else {
            if ("/".endsWith(requestedFile)) {
                requestedFile += DEFAULT_FILE;
            } if ("GET".equals(requestMethod)) {
                get.getMethod(out, requestedFile);
            }
        }
    }

    String getContentType(String requestedFile) {
        if (".html".endsWith(requestedFile))
            return "text/html";
        else
            return "text/plain";
    }

    String fileReader(String inputFileName) throws IOException {
        List<String> listOfStrings = Files.readAllLines(Paths.get("Resources", inputFileName.replace("/", "")));
        StringBuilder fileInString = new StringBuilder();
        for (String string : listOfStrings) {
            String[] arrayString = string.split(",");
            for (String s : arrayString) {
                fileInString.append(s.trim());
            }
        }
        return fileInString.toString();
    }

    byte[] readFile(File fileName) throws IOException {
        FileInputStream fileIn = new FileInputStream(fileName);
        return fileIn.readAllBytes();
    }
}
