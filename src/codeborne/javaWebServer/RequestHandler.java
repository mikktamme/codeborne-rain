package codeborne.javaWebServer;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class RequestHandler {

    String requestHandler(Socket socket) throws IOException {
       InputStream input = socket.getInputStream();

       String[] statusLine = splitHeaderLines(
               getHeader(
                       splitStringToHeadAndBody(
                               byteArrayToString(
                                       inputStreamToBytes(input)
                               )
                       )
               )
       );

       String[] newStatusLine = statusLine[0].split(" ");
//        String line;
//        List<String> requestLines = new ArrayList<>();
//
//        while ((line = in.readLine()) != null) {
//            if (line.isEmpty()) {
//                break;
//            }
//            requestLines.add(line);
//        }
//        return requestLines.size() > 0 ? requestLines.get(0) : "";
        return newStatusLine[0];
    }

    byte[] inputStreamToBytes(InputStream input) throws IOException {
        byte[] inputToBytes;
        try (input) {
            inputToBytes = input.readAllBytes();
        }
        return inputToBytes;
    }

    String byteArrayToString(byte[] input) {
        return new String(input);
    }

    String[] splitStringToHeadAndBody(String input) {
        return input.split("\r\n\r\n");
    }

    String getHeader(String[] input) {
        return input[0];
    }

    String getBody(String[] input) {
        return input[1];
    }

    String[] splitHeaderLines(String input) {
        return input.split("\r\n");
    }
}

