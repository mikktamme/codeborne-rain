package codeborne.javaWebServer;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class GET {
    ResponseHandler response = new ResponseHandler();

    void getMethod(PrintWriter out, String requestedFile) throws IOException {
        File file = new File(JavaWebServer.WEB_ROOT, requestedFile);
        int fileLength = (int) file.length();
        String type = response.getContentType(requestedFile);
        String bodyData = String.valueOf(response.fileReader(requestedFile));
        String outHeadAndBody = new Response1
                (
                        "200 OK",
                        type,
                        fileLength,
                        bodyData
                ).responseHeaderAndBody();
        out.write(outHeadAndBody);
    }
}
