package codeborne.week2;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day10AdventDay3MapTest {
    Day10AdventDay3Map advent = new Day10AdventDay3Map();


    @Test
    void testDistance() {
        assertEquals(3, advent.shortestDistance(Map.of("1, 0", 1, "2, 0", 2, "2, 1", 3, "2, 2", 4, "2, 3", 5,
                "1, 3", 6, "0, 3", 7, "-1, 3", 8, "-1, 2", 9, "-2, 2", 10),
                Map.of("0, -1", 1, "0, -2", 2, "-1, -2", 3, "-1, -1", 4, "-1, 0", 5,
                        "-1, 1", 6, "-1, 2", 7, "-1, 3", 8, "-1, 4", 9, "-1, 5", 10)));
    }
    @Test
    void testDistanceWithZeroInput() {
        assertEquals(1, advent.shortestDistance(Map.of("0", 0), Map.of("0", 0)));
    }
    @Test
    void testDistanceWithWrongInput() {
        assertEquals(0, advent.shortestDistance(Map.of("20", 0), Map.of("30", 0)));
    }
    @Test
    void testDistanceWithEmptyMap() {
        assertEquals(0, advent.shortestDistance(Map.of(), Map.of()));
    }
    @Test
    void testDistanceWithEmptyString() {
        assertEquals(2, advent.shortestDistance(Map.of("", 1), Map.of("", 2)));
    }

    @Test
    void testLength() {
        assertEquals(16, advent.shortestWireLength(Map.of("1, 0", 1, "2, 0", 2, "2, 1", 3, "2, 2", 4, "2, 3", 5,
                "1, 3", 6, "0, 3", 7, "-1, 3", 8, "-1, 2", 9, "-2, 2", 10),
                Map.of("0, -1", 1, "0, -2", 2, "-1, -2", 3, "-1, -1", 4, "-1, 0", 5,
                        "-1, 1", 6, "-1, 2", 7, "-1, 3", 8, "-1, 4", 9, "-1, 5", 10)));
    }
    @Test
    void testLengthWithZeroInput() {
        assertEquals(0, advent.shortestWireLength(Map.of("0", 0), Map.of("0", 0)));
    }
    @Test
    void testLengthWithWrongInput() {
        assertEquals(0, advent.shortestWireLength(Map.of("45", 1), Map.of("90", 1)));
    }
    @Test
    void testLengthWithEmptyMap() {
        assertEquals(0, advent.shortestWireLength(Map.of(), Map.of()));
    }
    @Test
    void testLengthWithEmptyString() {
        assertEquals(0, advent.shortestWireLength(Map.of("", 0), Map.of("", 0)));
    }
}