package codeborne.week1;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class Day01FactorialTest {

    @Test
    public void calcFactorialIsZero() {
        BigInteger isZero = Day01Factorial.calcFactorial(0);
        BigInteger results = BigInteger.valueOf(1);
        assertEquals(results, isZero);
    }
    @Test
    public void calcFactorialIsOne() {
        BigInteger isOne = Day01Factorial.calcFactorial(1);
        BigInteger results = BigInteger.valueOf(1);
        assertEquals(results, isOne);
    }
    @Test
    public void calcFactorialIsNegative() {
        BigInteger isNegative = Day01Factorial.calcFactorial(-2);
        assertNull(isNegative);
    }
    @Test
    public void testIfFactorialIsCorrect() { // Pm näide kuidas peaks olema vs see mmis alguses tegin.
        assertEquals(Day01Factorial.calcFactorial(5), BigInteger.valueOf(120));
    }
}