package codeborne.week1;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day01ListSortingTest {

    @Test
    void testIfListIsSortedCorrectly() {
//        assertEquals(Arrays.toString(Day01ListSorting.sortingNumbers().toArray()), "[1, 6, 6, 7, 12, 21, 45, 100]");
        assertEquals(List.of(1, 6, 6, 7, 12, 21, 45, 100), Day01ListSorting.sortingNumbers(new ArrayList<>(List.of(12, 45, 1, 6, 7, 6, 100, 21))));
    }
    @Test
    void testIfListIsEmpty() {
        assertEquals(new ArrayList<>(Collections.emptyList()), Day01ListSorting.sortingNumbers(new ArrayList<>(Collections.emptyList())));
    }
    @Test
    void testWithNegativeValues() {
        assertEquals(List.of(-100, -1, 0, 5, 12, 144), Day01ListSorting.sortingNumbers(List.of(144, 12, -100, -1, 0, 5)));
    }
}